using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Button = UnityEngine.UIElements.Button;
using Image = UnityEngine.UIElements.Image;

public class TilemapEditor : EditorWindow
{
    private VisualElement _root;
    private VisualElement _mainPanel;
    private VisualElement _rightPanel;
    private VisualElement _leftPanel;
    private VisualElement _topLeftPanel;
    private VisualElement _downLeftPanel;

    private bool _isDrawCell = false;
    private TileData _curTileData;
    private TilemapData _curTilemapData;
    private TextField _tilemapName;
    private List<Box> _gridList = new List<Box>();
    private Sprite _curTileSprite;

    [MenuItem("Tools/Tilemap Editor")]
    public static void TilemapWindow()
    {
        var window = GetWindow<TilemapEditor>(EditorConst.WindowName);
        window.maxSize = new Vector2(EditorConst.WindowWidth, EditorConst.WindowHeight);
        window.minSize = new Vector2(EditorConst.WindowWidth, EditorConst.WindowHeight);
        EditorActions.LoadAllTilemap();
        EditorActions.LoadAllTiles();
        // window.position = Rect.zero;
    }

    private void DrawMainPanel()
    {
        _mainPanel = EditorElement.CreatePanel(FlexDirection.Row, true, true);
        _root.Add(_mainPanel);
    }

    private void DrawMenu()
    {
        var toolbar = new Toolbar();
        _root.Add(toolbar);

        var loadAsset = new ToolbarButton(() =>
        {
            _curTilemapData = EditorActions.LoadTilemap();
            _tilemapName.value = _curTilemapData.name;
            LoadGrid();
        })
        {
            text = "Load Tilemap"
        };
        toolbar.Add(loadAsset);

        var saveAsset = new ToolbarButton(EditorActions.SaveTilemap)
        {
            text = "Save Tilemap"
        };
        toolbar.Add(saveAsset);
    }

    private void DrawLeftPanel()
    {
        _leftPanel = EditorElement.CreatePanel(FlexDirection.Column, true, false);
        _mainPanel.Add(_leftPanel);

        DrawTopLeft();
        DrawDownLeft();
    }

    private void DrawTopLeft()
    {
        _topLeftPanel = EditorElement.CreatePanel(FlexDirection.Column, true, true);
        _leftPanel.Add(_topLeftPanel);

        _tilemapName = new TextField("Tilemap Name")
        {
            value = "",
            style =
            {
                maxWidth = 550
            }
        };
        _topLeftPanel.Add(_tilemapName);
        var createTilemapBtn = new Button(() =>
        {
            _curTilemapData = EditorActions.CreateTilemap(_tilemapName.text);
        });
        createTilemapBtn.text = "Create Tilemap";
        _topLeftPanel.Add(createTilemapBtn);

        var objField = new ObjectField("Selected Tile : ");
        objField.objectType = typeof(TileData);
        objField.RegisterValueChangedCallback(evt => _curTileData = (TileData)evt.newValue);
        _topLeftPanel.Add(objField);

        var operationPanel = EditorElement.CreatePanel(FlexDirection.Row, false, false);
        _topLeftPanel.Add(operationPanel);
        var drawBtn = new Button(() => _isDrawCell = true)
        {
            text = "Draw Cell"
        };
        operationPanel.Add(drawBtn);
        var eraseBtn = new Button(() => _isDrawCell = false)
        {
            text = "Erase Cell"
        };
        operationPanel.Add(eraseBtn);
    }

    private void DrawDownLeft()
    {
        _downLeftPanel = EditorElement.CreatePanel(FlexDirection.Column, true, true);
        _leftPanel.Add(_downLeftPanel);

        var tileList = new ListView(EditorActions.EditorData.TilesList, 15, null, null);
        _downLeftPanel.Add(tileList);

        var tile = new TextField("Tile Name")
        {
            value = "",
            style =
            {
                maxWidth = 550
            }
        };
        _downLeftPanel.Add(tile);

        var objField = new ObjectField("Selected Tile Sprite : ")
        {
            objectType = typeof(Sprite)
        };
        objField.RegisterValueChangedCallback(evt => _curTileSprite = (Sprite)evt.newValue);
        _downLeftPanel.Add(objField);

        var createTileBtn = new Button(() => EditorActions.CreateTile(tile.text, _curTileSprite))
        {
            text = "Create Tile"
        };
        _downLeftPanel.Add(createTileBtn);
    }

    private void DrawRightPanel()
    {
        _rightPanel = EditorElement.CreatePanel(FlexDirection.Column, true, true);
        _mainPanel.Add(_rightPanel);

        for (int row = 0; row < EditorConst.GridRow; row++)
        {
            var rowPanel = EditorElement.CreatePanel(FlexDirection.Row, false, false);
            _rightPanel.Add(rowPanel);
            for (int col = 0; col < EditorConst.GridCol; col++)
            {
                var box = EditorElement.CreateBox($"{row}_{col}", Color.grey);
                box.RegisterCallback<MouseUpEvent>(OnMouseUpEvent);
                rowPanel.Add(box);
                _gridList.Add(box);
            }
        }
    }

    private void OnMouseUpEvent(MouseUpEvent e)
    {
        if (_curTilemapData == null)
        {
            return;
        }

        var image = new Image();
        if (_isDrawCell)
        {
            image.style.flexShrink = 1.0f;
            if (_curTileData != null)
            {
                image.image = _curTileData.TileRes.texture;
                if (e.target is Box box && image.image != null)
                {
                    box.Add(image);
                    var boxName = box.name;
                    var pos = boxName.Split('_');
                    EditorActions.AddTileToTilemap(_curTileData, int.Parse(pos[0]), int.Parse(pos[1]));
                }
            }
        }
        else
        {
            var target = (Image)e.target;
            if (target.parent != null)
            {
                var box = target.parent;
                box.Clear();
                var pos = box.name.Split('_');
                EditorActions.RemoveTileFromTilemap(int.Parse(pos[0]), int.Parse(pos[1]));
            }
        }
    }

    private void LoadGrid()
    {
        for (int i = 0; i < _gridList.Count; i++)
        {
            _gridList[i].Clear();
        }

        var list = _curTilemapData.TileDataList;
        for (int i = 0; i < list.Count; i++)
        {
            var pos = $"{list[i].Position.x}_{list[i].Position.y}";
            for (int j = 0; j < _gridList.Count; j++)
            {
                if (_gridList[j].name == pos)
                {
                    var img = new Image();
                    img.image = list[i].TileData.TileRes.texture;
                    _gridList[j].Add(img);
                    break;
                }
            }
        }
    }

    private void CreateGUI()
    {
        _root = rootVisualElement;

        DrawMenu();
        DrawMainPanel();
        DrawLeftPanel();
        DrawRightPanel();
    }
}