using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class EditorElement
{
    public static VisualElement CreatePanel(FlexDirection direction, bool isSetGrow, bool isSetStyle)
    {
        var panel = new VisualElement
        {
            style =
            {
                flexDirection = direction
            }
        };

        if (isSetGrow)
        {
            panel.style.flexGrow = 1.0f;
        }

        if (isSetStyle)
        {
            SetStyle(panel, 1.0f, Color.gray);
        }

        return panel;
    }

    public static Box CreateBox(string name, Color color)
    {
        var box = new Box
        {
            style = {
                minWidth = EditorConst.CellSize,
                minHeight = EditorConst.CellSize,
                maxHeight = EditorConst.CellSize,
                maxWidth = EditorConst.CellSize
            },
            name = name
        };
        SetStyle(box, 1, color);
        return box;
    }

    public static void SetStyle(VisualElement element, float borderWidth, Color color)
    {
        element.style.borderTopWidth = borderWidth;
        element.style.borderBottomWidth = borderWidth;
        element.style.borderRightWidth = borderWidth;
        element.style.borderLeftWidth = borderWidth;

        element.style.borderTopColor = color;
        element.style.borderBottomColor = color;
        element.style.borderRightColor = color;
        element.style.borderLeftColor = color;
    }

    public static void ShowPopDialog(string title, string content, string ok = "OK", string cancel = "Cancel")
    {
        EditorUtility.DisplayDialog(title, content, ok, cancel);
    }
}