using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorConst
{
    public const string WindowName = "Tilemap Editor";
    public const int WindowWidth = 1280;
    public const int WindowHeight = 720;

    public const int GridCol = 10;
    public const int GridRow = 10;
    public const int CellSize = 64;

    public const string ImagesPath = @"Assets/Res";
    public const string TilesSavePath = @"Assets/TilemapEditor/Tiles";
    public const string TilemapSavePath = @"Assets/TilemapEditor/Tilemaps";
    public const string TilemapJsonPath = @"Assets/StreamingAssets/TilemapData";
}