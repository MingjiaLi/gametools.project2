using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class EditorData
{
    public Dictionary<string, TilemapData> TilemapDataDic = new Dictionary<string, TilemapData>();
    public List<TileData> TilesList = new List<TileData>();
    public TilemapData CurTilemapData;

    public void LoadAllTilemap()
    {
        TilemapDataDic.Clear();

        var path = EditorConst.TilemapSavePath;
        path = path.Replace("Assets", "");
        path = $"{Application.dataPath}/{path}";

        var allTilemap = Directory.GetFiles(path, "*.asset", SearchOption.AllDirectories);
        for (int i = 0; i < allTilemap.Length; i++)
        {
            var name = allTilemap[i];
            name = name.Replace("\\", "/");
            name = name.Substring(name.LastIndexOf("/", StringComparison.Ordinal) + 1);
            var assetPath = $"{EditorConst.TilemapSavePath}/{name}";
            var tilemap = AssetDatabase.LoadAssetAtPath<TilemapData>(assetPath);
            TilemapDataDic.Add(tilemap.name, tilemap);
        }
    }

    public TilemapData LoadTilemap(string name)
    {
        TilemapData data;
        if (!TilemapDataDic.TryGetValue(name, out data))
        {
            data = AssetDatabase.LoadAssetAtPath<TilemapData>($"{EditorConst.TilemapSavePath}/{name}");
            TilemapDataDic.Add(name, data);
        }

        CurTilemapData = data;
        return data;
    }

    public void LoadAllTiles()
    {
        TilesList.Clear();

        var path = EditorConst.TilesSavePath;
        path = path.Replace("Assets", "");
        path = $"{Application.dataPath}/{path}";

        var allTiles = Directory.GetFiles(path, "*.asset", SearchOption.AllDirectories);
        for (int i = 0; i < allTiles.Length; i++)
        {
            var name = allTiles[i];
            name = name.Replace("\\", "/");
            name = name.Substring(name.LastIndexOf("/", StringComparison.Ordinal) + 1);
            var assetPath = $"{EditorConst.TilesSavePath}/{name}";
            var tile = AssetDatabase.LoadAssetAtPath<TileData>(assetPath);
            TilesList.Add(tile);
        }

        Debug.Log("allTiles = " + allTiles.Length);
    }

    public TilemapData CreateTilemap(string name)
    {
        if (TilemapDataDic.ContainsKey(name))
        {
            EditorElement.ShowPopDialog("Warning", $"Map name : {name} already exists!");
            return null;
        }

        var tilemap = ScriptableObject.CreateInstance<TilemapData>();
        tilemap.TilemapName = name;
        AssetDatabase.CreateAsset(tilemap, $"{EditorConst.TilemapSavePath}/{name}.asset");
        AssetDatabase.Refresh();
        TilemapDataDic.Add(name, tilemap);
        CurTilemapData = tilemap;
        return tilemap;
    }

    public void SaveTilemap()
    {
        if (CurTilemapData == null)
        {
            EditorElement.ShowPopDialog("Warning", "Please open a tilemap first.");
            return;
        }

        var path = $"{EditorConst.TilemapJsonPath}/{CurTilemapData.name}.json";

        // var folder = path.Replace(Application.streamingAssetsPath, "");
        // if (!File.Exists($"{Application.streamingAssetsPath}{folder}/{CurTilemapData.name}.json"))
        // {
        //     File.Create($"{Application.streamingAssetsPath}{folder}/{CurTilemapData.name}.json");
        // }

        using StreamWriter w = new StreamWriter(path);
        string json = JsonUtility.ToJson(CurTilemapData);
        w.Write(json);
        w.Close();
        w.Dispose();

        AssetDatabase.Refresh();
    }
}