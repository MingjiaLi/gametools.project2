using UnityEngine;

[CreateAssetMenu]
public class TileData : ScriptableObject
{
    [Header("Tile Info")]
    public string TileName;

    public Sprite TileRes;
}