using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class EditorActions
{
    public static EditorData EditorData = new EditorData();

    public static TilemapData LoadTilemap()
    {
        var path = EditorUtility.OpenFilePanel("Select Tilemap", EditorConst.TilemapSavePath, "*asset");
        if (string.IsNullOrEmpty(path))
        {
            return null;
        }

        path = path.Replace(Application.dataPath, "");
        var index = path.LastIndexOf('/');
        path = path.Substring(index + 1);

        return EditorData.LoadTilemap(path);
    }

    public static void LoadAllTilemap()
    {
        EditorData.LoadAllTilemap();
    }

    public static void LoadAllTiles()
    {
        EditorData.LoadAllTiles();
    }

    public static void SaveTilemap()
    {
        EditorData.SaveTilemap();
    }

    public static TilemapData CreateTilemap(string mapName)
    {
        if (string.IsNullOrEmpty(mapName))
        {
            EditorElement.ShowPopDialog("Warning", "Please enter map name!");
            return null;
        }

        return EditorData.CreateTilemap(mapName);
    }

    public static void AddTileToTilemap(TileData tile, int row, int col)
    {
        if (tile == null)
        {
            return;
        }

        if (EditorData.CurTilemapData == null)
        {
            return;
        }

        var detail = new TilemapDataDetail
        {
            TileData = tile,
            Position = new Vector2(row, col)
        };
        EditorData.CurTilemapData.TileDataList.Add(detail);
    }

    public static void RemoveTileFromTilemap(int row, int col)
    {
        if (EditorData.CurTilemapData == null)
        {
            return;
        }

        var list = EditorData.CurTilemapData.TileDataList;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].Position.x == row && list[i].Position.x == col)
            {
                EditorData.CurTilemapData.TileDataList.RemoveAt(i);
                break;
            }
        }
    }

    public static void CreateTile(string name, Sprite sprite)
    {
        if (string.IsNullOrEmpty(name))
        {
            EditorElement.ShowPopDialog("Warning", "Please enter tile's name.");
            return;
        }

        var tile = ScriptableObject.CreateInstance<TileData>();
        tile.TileName = name;
        tile.TileRes = sprite;
        AssetDatabase.CreateAsset(tile, $"{EditorConst.TilesSavePath}/{name}.asset");
        AssetDatabase.Refresh();
        EditorActions.EditorData.TilesList.Add(tile);
    }
}