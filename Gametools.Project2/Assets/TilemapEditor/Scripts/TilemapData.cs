using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TilemapData : ScriptableObject
{
    [Header("Tilemap Info")]
    public string TilemapName;

    [Header("Tilemap Data")]
    public List<TilemapDataDetail> TileDataList;
}

[Serializable]
public class TilemapDataDetail
{
    public TileData TileData;
    public Vector2 Position;
}